# esqueleto
Wordpress template ready to use with Elementor, absolutely from scratch.

The purpose of this template is to help all those design artisans, who like to have control of everything they use in their web designs.
Of course they will control everything from Elementor since "Esqueleto Template" is just an absolutely empty template with the minimum to work, where they can express all their creativity. Let's paint with your designs! On this empty canvas dear friends.

<img src="https://downloader.disk.yandex.ru/preview/055e2fa84cd5f4cf5da57f7362616c4e49a2c5c48847e5f293914906435ed4ae/5e84e5f2/wzGcpw614jC7Tm2a7yBFJfT8zzqfYSyGc_QDYabQaZTcZuJgeNb9oNs3aXK2h4EHZieuOK_EHwPuO20JWvoeFw==?uid=0&filename=screenshot.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&tknv=v2&owner_uid=900122159&size=2048x2048">


<a href="mailto:claciudad@yandex.xom">Martin Oviedo</a>


